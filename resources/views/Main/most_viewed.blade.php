@extends('Main.layouts.app')

@section("content")
                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
                    <div class="center_container">
                        <div class="card rgba-grey-light">
                            <div class="card-block cardupdate">
                                <h2 class="h2update">Most Viewed Wallpaper</h2>
                            </div>
                        </div>
            <br>
                        <script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- wallpaper theme new boot link -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="{{ config('global.adsense') }}"
                             data-ad-slot="3554802685"
                             data-ad-format="link"></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>          

                        <div class="row">
                            @if( isset($most_viewed) )
                            @foreach( $most_viewed as $val )
                            <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                                <div class="card">
                                    <div class="view overlay hm-white-slight">
                                        <a href="{{ url('/wp/') . '/' . $val->id . '/' . $val->slug }}">
                                        <div class="thumbnail">
                                            <img src="{{ asset( $val->image_thumb ) }}" alt="{{ substr($val->image_title, 0, 25) }}..." width="100%" height="100%"/> 
                                            <div class="caption">
                                                {{ substr($val->image_title, 0, 25)}}...                             
                                            </div> 
                                        </div>
                                        </a>
                                    </div>
                                </div>
                            <br>
                            </div>
                            @endforeach
                            @endif
                        </div>
                        <nav>
                            <ul class="pagination flex-center pg-dark">
                                {{ $most_viewed->onEachSide(1)->links() }}
                            </ul>
                        </nav>
                    </div>
                </div>
@endsection

@section("js")

@endsection

@section("ajaxandscripts")

@endsection