    <div class="container-fluid containerhd" >
      <header>
         <nav class="navbar navbar-toggleable-md navbar-dark elegant-color-dark fixed-top navbarcu">
         <a class="navbar-brand" href="index.html" title="wallpapers HD"><b>WALLPAPERS HD</b></a>   
         <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav1" aria-controls="navbarNav1" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav1">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">&nbsp;<i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ url('latest') }}">&nbsp;<i class="fa fa-signal" aria-hidden="true"></i> New Wallpapers</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ url('most-viewed') }}">&nbsp;<i class="fa fa-star" aria-hidden="true"></i> Most Viewed</a>
                  </li>
                </ul>
               <form class="form-inline waves-effect waves-light" action="{{ url('/search/') }}" method="GET">
                  <div class="row">
                     <div class="col-md-8">
                        <input name="task" type="hidden" value="search" />
                        <input name="query" class="form-control" type="text" placeholder="Search" size="20" id="search_textbox" value="Search..." onclick="clickclear(this, 'Search...')" onblur="clickrecall(this,'Search...')"/> 
                     </div>
                     <div class="col-md-4">
                        <div class="text-center">
                           <input type="image" class="buttonse" src="{{ asset('main/images/search_button.png') }}" />
                        </div>
                     </div>
                  </div>
               </form>  
          </div>
         </nav>
         <!-- Menu End here-->
      </header>
    </div>