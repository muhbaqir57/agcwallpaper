<head>
      <title>{{ $page_title ?? config('global.title') }}</title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <meta name="description" content="{{ $meta['description'] ?? config('global.meta.description') }}" />
      <meta name="keywords" content="{{ $meta['keywords'] ??  config('global.meta.keywords') }}" />
      <meta name="distribution" content="Global" />
      
      <meta name="rating" content="General" />
      <meta name="language" content="en-us" />
      <meta name="robots" content="index,follow" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css"/>
      <link href="{{ asset('main/css/bootstrap.min.css') }}" rel="stylesheet"/>
      <link href="{{ asset('main/css/mdb.min.css') }}" rel="stylesheet"/>
      <link href="{{ asset('main/css/style.css') }}" rel="stylesheet"/>
      <link href="{{ asset('main/css/button.css') }}" rel="stylesheet"/>
      <link rel="alternate" type="application/rss+xml" title="{{ url('/') }}" href="rss.php" />

      <link rel="shortcut icon" href="{{ asset('favicon.png') }}" type="image/x-icon" />
      <link rel="icon" href="{{ asset('favicon.png') }}" type="image/x-icon" />

      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-0R6L46DCWH"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-0R6L46DCWH');
      </script>
      
      <script type="text/javascript" src="{{asset('main/js/wss.js')}}"></script>

      <script data-ad-client="{{ config('global.adsense') }}" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>