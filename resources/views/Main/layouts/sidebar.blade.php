					<div class="category_menu_item">
                        <a class="list-group-item list-group-item-action catmodules text-uppercase" href="{{ url('categories/all') }}">All Categories</a>
                    </div>
                    @if( isset( $categories ) )
					@foreach( $categories as $cat)
					<div class="category_menu_item">
                        <a class="list-group-item list-group-item-action catmodules text-uppercase" href="{{ url('categories/') . '/' . $cat->categories_slug }}">{{ $cat->categories_name }}</a>
                    </div>
                    @endforeach
                    @endif