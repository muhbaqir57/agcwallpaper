<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

@include('Main.layouts.head')

<body>
     <!-- Histats.com  START  (aync)-->
    <script type="text/javascript">var _Hasync= _Hasync|| [];
    _Hasync.push(['Histats.start', '1,4504547,4,0,0,0,00010000']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    (function() {
    var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
    hs.src = ('//s10.histats.com/js15_as.js');
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
    })();</script>
    <noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?4504547&101" alt="counter" border="0"></a></noscript>
    <!-- Histats.com  END  -->
    @include('Main.layouts.header')
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                    <script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- wallpaper theme new boot link -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="{{ config('global.adsense') }}"
                         data-ad-slot="3554802685"
                         data-ad-format="link"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>

                    @include('Main.layouts.sidebar')
                    
    <br>        </div>
    @yield('content')
                <!-- Central Modal Medium Warning -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-notify modal-warning" role="document">
                        <!--Content-->
                        <div class="modal-content ">
                            <div class="modal-body" style="padding-left: 0px;padding-right: 0px;padding-bottom: 0px; padding-top: 0px;">
                                <div class="text-center">
                                    <i class="fa fa-check fa-4x mb-1 animated rotateIn"></i>
                                    <p>AdBlock is Enabled! <br> You made our site admin unhappy.<br> Please disable AdBlock to continue using the best Wallpapers<hr> Note:- After disable refresh website </p>
                                    <img src="https://www.wallpaper.net.in/templates/Responsive/images/cat33.jpg" class="img-fluid" alt="Responsive image">
                                </div>
                            </div>
                        </div>
                        <!--/.Content-->
                    </div>
                </div>
                <!-- Central Modal Medium Warning-->
            </div>
        </div>
    </main>

    <footer class="page-footer center-on-small-only navbar-dark elegant-color-dark" style="margin-top: 0px;padding-top: 0px;">
        <div class="footer-copyright">
             Powered by <a href="{{ url('/') }}">{{ config("global.title") }}</a> || <a href="{{ url('page/privacy-policy') }}">Privacy policy</a> || <a href="{{ url('page/dmca') }}">DMCA</a> ||<a href="{{ url('page/terms-of-use') }}"> Terms of Use</a> || <a href="{{ url('page/cookie-policy') }}">Cookie Policy</a>
         </div>
        <!--/.Copyright-->

    </footer>
    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="{{asset('main/js/jquery-2.2.3.min.js')}}"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{asset('main/js/tether.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('main/js/ads.js')}}"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{asset('main/js/bootstrap.min.js')}}"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{asset('main/js/mdb.min.js')}}"></script>
    
    @yield("js")

    <script>
        new WOW().init();
    </script>

    <script type="text/javascript">
        if( window.canRunAds === undefined ){
            $(window).on('load',function(){
                $('#myModal').modal('show');
            });
        }

        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }

        $(document).ready(function() {
            $("[rel='tooltip']").tooltip();    
         
            $('.thumbnail').hover(
                function(){
                    $(this).find('.caption').slideDown(350); //.fadeIn(250)
                },
                function(){
                    $(this).find('.caption').slideUp(150); //.fadeOut(205)
                }
            ); 
        });

        @yield("ajaxandscripts")
    </script>
</body>
</html>