@extends('Main.layouts.app')

@section('content')
	<div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
        <div class="center_container">
        	{!! $page[0]->page_content !!}
       	</div>
    </div>
@endsection


@section("js")

@endsection

@section("ajaxandscripts")

@endsection