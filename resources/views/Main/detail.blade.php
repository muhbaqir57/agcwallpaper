@extends('Main.layouts.app')

@section("content")
    <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 col-lg-9uphe">
        <ol class="breadcrumb">
            <a href="{{ url('/') }}">Home</a> &raquo; 
            <a href="{{ url('/') . '/' . $image[0]->Categories->categories_name }}">
                {{ $image[0]->Categories->categories_name }}
            </a> &raquo;  
            {{ $image[0]->image_title }}
        </ol>
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Newvideostatus -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="{{ config('global.adsense') }}"
             data-ad-slot="3799787241"
             data-ad-format="auto"
             data-full-width-responsive="true"></ins>
        <script>
             (adsbygoogle = window.adsbygoogle || []).push({});
        </script>                            
        <div class="row">
            <div class="col-12">
                <div class="carousel-caption">
                    <div class="fb-like" data-href="{{ url()->current() }}" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
                </div>
                <img class="img-thumbnail" src="{{ asset( 'storage/' .  $image[0]->image_path ) }}" alt="{{ $image[0]->image_title}}" width="100%" height="100%"/>
            </div>
        </div>            
  
        <script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- wallpaper theme new boot link -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="{{ config('global.adsense') }}"
             data-ad-slot="3554802685"
             data-ad-format="link"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>                
        <br>
        <div class="card">
            <span class="card-header stylish-color white-text">Wallpaper Details</span>
            <div class="card-block">
                <dl class="row">
                  <dd class="col-sm-12"><h3>{{ $image[0]->image_title }}</h3></dd>
                    <br>
                        <dd class="col-sm-12">20-01-16</dd>
                    <br>
                        <dd class="col-sm-12"><p class="text-justify" >Download {{ $image[0]->image_title }} and background with more  wallpaper collection or Full HD 1080p Desktop Background for any Computer, Laptop, Tablet and Phone</p></dd>
                    <br>
                        <dd class="col-sm-12">
                            @foreach( $image[0]->tags as $tag )
                            <a class="badge badge-primary" href="{{ url('/tag/') . '/' . $tag->slug }}">{{ $tag->name }}</a>&nbsp;
                            @endforeach
                        </dd>
                    <br>
                    <dd class="col-sm-12"><i class="glyphicon glyphicon-search"></i>Views:</span> {{ $image[0]->view_count }} 
                        <span class="info_item_title">Downloads:</span> {{ $image[0]->download_count }}
                    </dd>
                </dl>
            </div>
        </div>
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Newvideostatus -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="{{ config('global.adsense') }}"
             data-ad-slot="3799787241"
             data-ad-format="auto"
             data-full-width-responsive="true"></ins>
        <script>
             (adsbygoogle = window.adsbygoogle || []).push({});
        </script>            
        <br>
        <div class="card">
            <span class="card-header stylish-color white-text">More Love wallpapers</span>
            <div class="card-block">
                <div class="row">
                    @foreach( $similar as $val )
                    <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                        <div class="card">
                            <div class="view overlay hm-white-slight">
                                <a href="{{ url('/wp') . '/' . $val->id . '/' . $val->slug }}">
                                <div class="thumbnail">
                                    <img src="{{ asset( $val->image_thumb ) }}" alt="{{ substr($val->image_title, 0, 25)}}..." width="100%" height="100%"/> 
                                    <div class="caption">
                                        {{ substr($val->image_title, 0, 25)}}...                              
                                    </div> 
                                </div>
                                </a>
                            </div>
                        </div>
                        <br>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>  
    </div>
@endsection