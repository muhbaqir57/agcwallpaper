<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table 		=	"page";
    protected $primary_key	= 	"id";
}
