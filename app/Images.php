<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    use \Conner\Tagging\Taggable;
    
    protected $table        =   "images";
    protected $primary_key  =   "id";

    public function categories()
    {
        return $this->belongsTo('App\Categories');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
