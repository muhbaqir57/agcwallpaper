<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class derived_keywords extends Model
{
    
    protected $table        =   "derived_keywords";
    protected $primary_key  =   "id";

    public function keywords()
    {
        return $this->belongsTo('App\Keywords');
    }

    public function categories()
    {
        return $this->belongsTo('App\Categories', 'foreign_key');
    }
}
