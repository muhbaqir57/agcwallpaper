<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categories;
use App\Images;

class SearchController extends Controller
{
    protected $categories;

    public function __construct()
    {
    	$this->categories     = Categories::orderBy("categories_name")
                                ->take(10)
                                ->get();	
    }

    public function index()
    {

    }

    public function search( Request $request )
    {
    	$categories 	=	$this->categories;

    	$query 		=	$request->get("query");

    	$search 	=	Images::Join("categories", "categories.id", 'images.categories_id')
    					->where("images.image_title", "LIKE", "%" . $query . "%")
    					->orWhere("categories.categories_name", "LIKE", "%" . $query . "%")
    					->paginate(9);

    	return view("Main/search")->with( compact("categories", "query", "search") );
    }
}
