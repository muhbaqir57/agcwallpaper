<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Images;
use App\Categories;


class HomeController extends Controller
{
    private $categories;

    public function __construct()
    {
        $this->categories     = Categories::orderBy("categories_name")
                                ->take(10)
                                ->get();
    }

    public function index()
    {
        $categories     =   $this->categories;

        $most_viewed    =   Images::orderBy("view_count", "desc")
                            ->take(6)
                            ->get();
        
        $latest         =   Images::orderBy("created_at", "desc")
                            ->paginate(9);

        return view("Main/home")->with( 
                                            compact("most_viewed", "latest", "categories")
                                        );
    }

    public function wp( $id, $slug = "" )
    {
        $categories     =   $this->categories;

        $image  =   Images::where( "id", $id )
                    ->with("Categories")
                    ->get();

        if( $image->isEmpty() )
        {
            abort( 404 );
        }

        $page_title     =   $image[0]->image_title . " - " . config("global.title");

        $similar    =   Images::where("categories_id", $image[0]->categories_id )
                        ->take(6)
                        ->orderBy("view_count", "desc")
                        ->get();

        $meta       =   [
                            "description"   => "Download ". $image[0]->image_title ." and background with more wallpaper collection or Full HD 1080p Desktop Background for any Computer, Laptop, Tablet and Phone",
                            "keywords"      => ""
                        ];

        foreach ($image[0]->tags as $tag) {
             $meta['keywords'] .= $tag->name . ",";
        }

        //Increment view count
        $image[0]->increment("view_count", 1);
        
        return view( "Main/detail")->with(
                                            compact( "page_title", "image", "similar", "categories", "meta" )
                                        );
    }
}
