<?php

namespace App\Http\Controllers\Mdwp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Buchin\GoogleImageGrabber\GoogleImageGrabber as GIG;

use App\Keywords;
use App\derived_keywords;
use App\Images;

use Storage;

use Image as Resizer;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $derived_keywords   =  derived_keywords::all()->where('status', FALSE);

        $data   =   [];
        $ua     = \Campo\UserAgent::random([
                    'os_type' => ['Windows', 'OS X', 'Linux'],
                    'device_type' => 'Desktop'
                ]);

        $options  = [
            'http' => [
                'method'     =>"GET",
                'user_agent' =>  $ua,
            ],
            'ssl' => [
                "verify_peer"      => FALSE,
                "verify_peer_name" => FALSE,
            ],
        ];

        foreach ($derived_keywords as $val_0) {
            try {
                $data   =   GIG::grab($val_0->derived_keywords);
                
                foreach ($data as $key => $val_1) {
                    // Download image by derived keywords
                    $context    =   stream_context_create($options);
                    $image      =   file_get_contents( $val_1['url'], FALSE, $context );   
                    $keyword    =   str_replace(" ", "-", $val_1['keyword']);
                    $name       =   $val_1['slug'] . "_" . str_replace(" ", "-", $val_1['title']) . "." . $val_1['filetype'];
                    $dir        =   Storage::disk('public')->makeDirectory( $keyword );
                    $image_path =   $keyword . "/" . $name;
                    $tags       =   str_replace(" ", ", ", $val_1['title']) . ', ' . str_replace(" ", ", ", $val_1['keyword']);

                    // Save to storage
                    Storage::disk('public')->put( $image_path, $image );

                    // Insert path to table Images
                    $img    =   new Images;

                    $img->image_title   =   $val_1['title'];
                    $img->image_path    =   $image_path;
                    $img->image_thumb   =   "";
                    $img->categories_id =   $val_0->categories_id;
                    $img->download_count=   0;
                    $img->view_count    =   0;
                    $img->slug          =   \Str::slug( $img->image_title );

                    $img->save();

                    $img->tag( explode( ",", $tags ) );
                    $img->save();

                    // Create thumbnail
                    $thumb  =   $this->resize_image( $image, $keyword, $val_1['filetype'], $img->id );
                    $img->image_thumb   =   $thumb;
                    $img->save();

                    // Update derived keywords status
                    derived_keywords::where('id', $val_0->id)->update(['status' => TRUE ]);                    
                }
            } catch (\Throwable $e) {
                //return response( $e );
                continue;
            }

            // Update main keywords status
            Keywords::where('id', $val_0->id_keywords)->update(['status' => TRUE ]);
        }

        return view("Mdwp/Post");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generate()
    {

    }

    public function resize_image($image, $keyword, $filetype, $img_id)
    {
        $dir    =   Storage::disk('public')->makeDirectory( "thumbnails/" . $keyword . "/". $img_id );
        $str_path   =   "storage/thumbnails/" . $keyword . "/". $img_id . "/350x220." . $filetype;
        $path       =   public_path("storage/thumbnails/" . $keyword . "/". $img_id . "/350x220." . $filetype);

        $canvas =   Resizer::canvas( 350, 220 );
        $img    =   Resizer::make( $image )->resize( null, 220, function( $constraint ){
                                                                    $constraint->aspectRatio();
                                                                });

        $canvas->insert( $img, 'center' );
        $canvas->save( $path, 70);

        return $str_path;        
    }

    private function remove_duplicate( $str )
    {
        $str   =   implode(',', array_unique( explode(',', $str) ));

        return $str;
    }
}
