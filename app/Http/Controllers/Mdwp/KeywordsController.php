<?php

namespace App\Http\Controllers\Mdwp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Buchin\GoogleImageGrabber\GoogleImageGrabber as GIG;
use App\Keywords;
use App\derived_keywords;

class KeywordsController extends Controller
{   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories     =   \App\Categories::all();

        return view( "Mdwp/Keywords")->with( compact( "categories" ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $keywords   =   preg_split('/\r\n|[\r\n]/', $request->main_keywords );
        
        // Get keyword suggestion from main keywords
        foreach ($keywords as $value) {
            $keyword        =   new Keywords;
            $keyword->main_keywords     =   $value;
            $keyword->categories_id     =   $request->categories;
            $keyword->status            =   false;
            $keyword->save();

            $data   =   [];

            try {
                $data     =  GIG::suggested_keywords( $value . " Wallpaper" );
            } catch( Throwable $e ) {
                return response()->json( "failed" );
            }

            for( $i = 1; $i < count( $data[1] ) - 2 ; $i++)
            {
                $derived    =    new derived_keywords;
                $derived->id_keywords       =   $keyword->id;
                $derived->categories_id     =   $request->categories;
                $derived->status            =   false;
                $derived->derived_keywords  =   $data[1][$i];
                $derived->save();
            }

            sleep(5);
        }
        

        return response()->json( "success" );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_datatables( Request $request )
    {

    }
}
