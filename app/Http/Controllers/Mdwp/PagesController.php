<?php

namespace App\Http\Controllers\Mdwp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
    	return view("Mdwp/Pages");
    }
}
