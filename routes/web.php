<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix("mdwp")->group( function(){
    Auth::routes();
    Route::get("/",     "Mdwp\HomeController@index");
    Route::get("/home", "Mdwp\HomeController@index");

    // Route for post
    Route::resource("/post",     "Mdwp\PostController");
    Route::get("/post/generate", "Mdwp\PostController@generate");

    // Route for keywords
    Route::resource("/keywords", "Mdwp\KeywordsController");

    // Route for categories
    Route::resource("/categories",  "Mdwp\CategoriesController");

    //Route for pages
    Route::resource("/pages",  "Mdwp\PagesController");
});

// Route for Main
Route::get("/", array( "as" => "home", "uses" => "Main\HomeController@index" ));
Route::get("/latest", "Main\PagesController@new_wallpaper");
Route::get("/most-viewed", "Main\PagesController@most_viewed");

Route::get("/search/", "Main\SearchController@search");
Route::get("/page/{pages}", "Main\PagesController@index");
Route::get("/wp/{id}/{slug?}", "Main\HomeController@wp");
Route::get("/tag/{tag}", "Main\TagsController@tag");
Route::get("/categories/{cat}", "Main\CategoriesController@index");